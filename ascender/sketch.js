var img_bg;
var img_serif;
var img_line;
var path = "http://escenaconsejo.org/p5/Typographic-Expressive-Words/ascender/assets/";

var scaling;
var newwidth,newheight;

var offsetSerif = 24;
var offsetLine;

var n;

var justFinished;
var timer;
var timerTarget;

var alph;

function preload(){
  img_bg = loadImage(path+"bg.png");
  img_serif = loadImage(path+"serif.png");
  img_line = loadImage(path+"line.png");
}

function setup() {
  var canvas = createCanvas(600,400);
  canvas.parent("ascender");
  background(255,0,0);
  
  scaling = width/img_bg.width;
  newwidth = width;
  newheight = height*scaling;
  
  offsetLine = offsetSerif + img_serif.height*scaling;
  
  n = 0;
  
  justFinished = true;
  
  timerTarget = 3000;
  
  alph = 255;
  
}

function draw() {
  background(255);
  noStroke();
  
    image(img_bg, 0, height-newheight, newwidth, newheight );
  
  // Draw stroke
  for(var i=0;i<n;i++){
    image(img_line,0,height-newheight+offsetLine-i, newwidth, 1);
  }
  // Draw serif
  image(img_serif,0,height-newheight+offsetSerif-n+1, newwidth, img_serif.height*scaling);
  
  fill(255,alph);
  rect(0,0,width,height)
  if(alph>0){
    alph -= 5;
  }
  
  if(n<height-newheight){
    n+=0.2;
  }
  else{
    if(justFinished){
      timer = millis();
      justFinished = false;
    }
    else if(millis()-timer>timerTarget){
     justFinished = true;
     n = 0;
     alph = 255;
    }
  }
}