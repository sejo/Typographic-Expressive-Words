var font;
var texto = "kerning-freak";
var h;
var widths = []; 
var positions = [];
var desiredSpace = [];

var minDesiredSpace = 0;
var maxDesiredSpace = 12;

var timer;
var timerTarget; 
var minTime = 2000; //ms
var maxTime = 5000; //ms

var remotepath = "http://escenaconsejo.org/p5/Typographic-Expressive-Words/kerning-freak/";

function preload(){
//  font = loadFont("Arvo-Regular.ttf");
  font = loadFont(remotepath+"Arvo-Regular.ttf");
}

function setup() {
  var canvas = createCanvas(600,400);
  canvas.parent("kerning");
  textFont(font);
  textSize(42);
  h = height/2; // general height
  
  timer = 0;
  timerTarget = 0;
  
  // For each character in texto
  for(var i=0; i<texto.length; i++) {
      // Save the width of the character
    widths.push(textWidth(texto.charAt(i)));
    
    // Create the array of positions
    positions.push(20*(i+1));
    
    // Set a desired space between letters
    desiredSpace.push(0);
  }
  
}

function draw() {
  background(255);
  var currentSpace;
  var error;
  // for each character in texto
  for(var i=0; i<texto.length; i++) {
    // draw the character
    text(texto.charAt(i),positions[i],h);
    
    // Control operations to move the characters
    if( i>0 ) {
      // calculate the current space between the prev character and itself
      currentSpace = positions[i]-positions[i-1]-widths[i-1];
      // calculate the difference between the desired space and the current space
      error = desiredSpace[i]-currentSpace;
      // update the current position proportionally to the error
      positions[i] += error*0.05;
    }
    
  }
  
  // update timer
  if( millis() - timer > timerTarget ){
    var nchanges = random(1,texto.length);
    var randomi;
    var newSpace = random(minDesiredSpace,maxDesiredSpace);
    for(var i = 0; i < nchanges ; i++){
      randomi = int(random(0,texto.length));
      desiredSpace[randomi] = newSpace;
    }
    timer = millis();
    timerTarget = random(minTime, maxTime);
  }
  
}
