
var kerning = function(p){

var font;
var texto = "kerning-freak";
var h;
var widths = []; 
var positions = [];
var desiredSpace = [];

var minDesiredSpace = 0;
var maxDesiredSpace = 12;

var timer;
var timerTarget; 
var minTime = 2000; //ms
var maxTime = 5000; //ms

var remotepath = "http://escenaconsejo.org/p5/Typographic-Expressive-Words/kerning-freak/";

p.preload = function(){
//  font = loadFont("Arvo-Regular.ttf");
  font = p.loadFont(remotepath+"Arvo-Regular.ttf");
};

p.setup = function() {
  var canvas = p.createCanvas(600,400);
//  canvas.parent("kerning");
  p.textFont(font);
  p.textSize(42);
  h = p.height/2; // general height
  
  timer = 0;
  timerTarget = 0;
  
  // For each character in texto
  for(var i=0; i<texto.length; i++) {
      // Save the width of the character
    widths.push(p.textWidth(texto.charAt(i)));
    
    // Create the array of positions
    positions.push(20*(i+1));
    
    // Set a desired space between letters
    desiredSpace.push(0);
  }
  
};

p.draw = function() {
  p.background(255);
  var currentSpace;
  var error;
  // for each character in texto
  for(var i=0; i<texto.length; i++) {
    // draw the character
    p.text(texto.charAt(i),positions[i],h);
    
    // Control operations to move the characters
    if( i>0 ) {
      // calculate the current space between the prev character and itself
      currentSpace = positions[i]-positions[i-1]-widths[i-1];
      // calculate the difference between the desired space and the current space
      error = desiredSpace[i]-currentSpace;
      // update the current position proportionally to the error
      positions[i] += error*0.05;
    }
    
  }
  
  // update timer
  if( p.millis() - timer > timerTarget ){
    var nchanges = p.random(1,texto.length);
    var randomi;
    var newSpace = p.random(minDesiredSpace,maxDesiredSpace);
    for(var i = 0; i < nchanges ; i++){
      randomi = p.int(p.random(0,texto.length));
      desiredSpace[randomi] = newSpace;
    }
    timer = p.millis();
    timerTarget = p.random(minTime, maxTime);
  }
  
};

};


var ascender = function(p){

var img_bg;
var img_serif;
var img_line;
var path = "http://escenaconsejo.org/p5/Typographic-Expressive-Words/ascender/assets/";

var scaling;
var newwidth,newheight;

var offsetSerif = 24;
var offsetLine;

var n;

var justFinished;
var timer;
var timerTarget;

var alph;

p.preload = function(){
  img_bg = p.loadImage(path+"bg.png");
  img_serif = p.loadImage(path+"serif.png");
  img_line = p.loadImage(path+"line.png");
};

p.setup = function() {
  var canvas = p.createCanvas(600,400);
//  canvas.parent("ascender");
  p.background(255,0,0);
  
  scaling = p.width/img_bg.width;
  newwidth = p.width;
  newheight = p.height*scaling;
  
  offsetLine = offsetSerif + img_serif.height*scaling;
  
  n = 0;
  
  justFinished = true;
  
  timerTarget = 3000;
  
  alph = 255;
  
};

p.draw = function() {
  p.background(255);
  p.noStroke();
  
    p.image(img_bg, 0, p.height-newheight, newwidth, newheight );
  
  // Draw stroke
  for(var i=0;i<n;i++){
    p.image(img_line,0,p.height-newheight+offsetLine-i, newwidth, 1);
  }
  // Draw serif
  p.image(img_serif,0,p.height-newheight+offsetSerif-n+1, newwidth, img_serif.height*scaling);
  
  p.fill(255,alph);
  p.rect(0,0,p.width,p.height)
  if(alph>0){
    alph -= 5;
  }
  
  if(n<p.height-newheight){
    n+=0.2;
  }
  else{
    if(justFinished){
      timer = p.millis();
      justFinished = false;
    }
    else if(p.millis()-timer>timerTarget){
     justFinished = true;
     n = 0;
     alph = 255;
    }
  }
};


};




var kern = new p5(kerning,"kerning");
var ascend = new p5(ascender,"ascender");
